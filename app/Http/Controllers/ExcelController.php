<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use PHPExcel_NamedRange, PHPExcel_Cell_DataValidation;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('welcome');

        // $excel = App::make('excel');

        // $data = array(
        //     array('data1', 'data2'),
        //     array('data3', 'data4')
        // );

        // Excel::create('Filename', function($excel) use($data) {

        //     $excel->sheet('Sheetname', function($sheet) use($data) {

        //         $sheet->fromArray($data);

        //     });

        // })->download('xls');

    }

    public function exportExcel()
    {
      
        \Excel::create('file', function($excel) {
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/NamedRange.php");
            require_once(base_path()."/vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell/DataValidation.php");
            
            $items = ['Pensil', 'Torpedo', 'Beras', 'Jagung'];
            $supplier = ['PT Baruna', 'PT Petruk', 'PT Semar', 'PT Bagong'];
            $harga = ['1000', '2000', '3000', '4000', '5000'];
            
            $excel->sheet('master sheet', function($sheet) use ($items, $supplier) {
                $sheet->protect('password');
                for($i = 1; $i <= count($items); $i++)
                {
                    $sheet->SetCellValue("F".$i, $items[$i-1]);

                }
                for($i = 1; $i <= count($supplier); $i++)
                {
                    $sheet->SetCellValue("G".$i, $supplier[$i-1]);

                }

                $sheet->_parent->addNamedRange(
                        new PHPExcel_NamedRange(
                        'countries', $sheet, 'A1:A2'
                        )
                );
                
               
                $sheet->_parent->addNamedRange(
                        new PHPExcel_NamedRange(
                        'UK', $sheet, 'F1:F'.count($items)
                        )
                );


                
                $sheet->_parent->addNamedRange(
                        new PHPExcel_NamedRange(
                        'SUP', $sheet, 'G1:G'.count($supplier)
                        )
                );
               
            });

            $excel->sheet('New sheet', function($sheet) use ($items, $supplier){
                $sheet->cells('A1:B1', function($cells) {
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                });
                $sheet->cells('B4:C4', function($cells) {
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                });
                // $sheet->cells('F1:G4', function($cells) {
                //     $cells->setFontColor('#ffffff');
                // });
                $sheet->SetCellValue("A1", "Tanggal");
                $sheet->SetCellValue("A2", date('d-m-Y'));
                $sheet->SetCellValue("B1", "Supplier");
                $sheet->SetCellValue("B4", "Barang");
                $sheet->SetCellValue("C4", "Harga");
                $sheet->SetCellValue("A2", "USA");

                
                // $sheet->SetCellValue("B1", "London");
                // $sheet->SetCellValue("B2", "Birmingham");
                // $sheet->SetCellValue("B3", "Leeds");
                

                // $sheet->SetCellValue("C1", "Atlanta");
                // $sheet->SetCellValue("C2", "New York");
                // $sheet->SetCellValue("C3", "Los Angeles");
                // $sheet->_parent->addNamedRange(
                //         new PHPExcel_NamedRange(
                //         'USA', $sheet, 'C1:C3'
                //         )
                // );
                for($i = 5; $i < 10; $i++)
                {
                    $objValidation = $sheet->getCell('B'.$i)->getDataValidation();
                    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    // $objValidation->setErrorTitle('Input error');
                    // $objValidation->setError('Value is not in list.');
                    // $objValidation->setPromptTitle('Pick from list');
                    // $objValidation->setPrompt('Please pick a value from the drop-down list.');
                    $objValidation->setFormula1('UK'); //note this!
                }
                                
                

                $obj = $sheet->getCell('B2')->getDataValidation();                
                $obj->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
                $obj->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                $obj->setAllowBlank(false);
                $obj->setShowInputMessage(true);
                $obj->setShowErrorMessage(true);
                $obj->setShowDropDown(true);
                // $objValidation->setErrorTitle('Input error');
                // $objValidation->setError('Value is not in list.');
                // $objValidation->setPromptTitle('Pick from list');
                // $objValidation->setPrompt('Please pick a value from the drop-down list.');
                $obj->setFormula1('SUP'); //note this!
            });
        })->download("xlsx");
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
